package com.atlassian.bitbucket.plugin.hooks.protectbranch.pull;

import com.atlassian.bitbucket.plugin.hooks.protectbranch.repository.TestRepositoryBuilder;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.stash.internal.pull.InternalPullRequestRef;
import com.atlassian.stash.internal.repository.InternalRepository;

public class TestPullRequestRefBuilder {

    private final InternalPullRequestRef.Builder builder;

    public TestPullRequestRefBuilder() {
        builder = new InternalPullRequestRef.Builder();
        builder.id("2353").repository((InternalRepository) TestRepositoryBuilder.DEFAULT_REPOSITORY);
    }

    public TestPullRequestRefBuilder id(String id) {
        builder.id(id);
        return this;
    }

    public TestPullRequestRefBuilder repository(Repository newRepository) {
        builder.repository((InternalRepository)newRepository);
        return this;
    }

    public TestPullRequestRefBuilder repository(TestRepositoryBuilder repoBuilder) {
        return repository(repoBuilder.build());
    }

    public PullRequestRef build() {
        return builder.build();
    }
}
