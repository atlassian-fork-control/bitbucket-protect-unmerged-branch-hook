package com.atlassian.bitbucket.plugin.hooks.protectbranch.repository;

import com.atlassian.bitbucket.plugin.hooks.protectbranch.project.TestProjectBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.util.TestBuilderUtils;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.stash.internal.project.InternalProject;
import com.atlassian.stash.internal.repository.InternalRepository;

public class TestRepositoryBuilder {

    public static final Repository DEFAULT_REPOSITORY = new TestRepositoryBuilder().build();

    private final InternalRepository.Builder builder;
    private String slug;

    public TestRepositoryBuilder() {
        builder = new InternalRepository.Builder();
        builder.id(654)
                .name("Repo")
                .scmId("git")
                .project((InternalProject) TestProjectBuilder.DEFAULT_PROJECT);
        slug = null;
    }

    public TestRepositoryBuilder project(TestProjectBuilder projectBuilder) {
        return project(projectBuilder.build());
    }

    public TestRepositoryBuilder project(Project project) {
        builder.project((InternalProject) project);
        return this;
    }

    public TestRepositoryBuilder name(String name) {
        builder.name(name);
        return this;
    }

    public TestRepositoryBuilder scmId(String scmId) {
        builder.scmId(scmId);
        return this;
    }

    public TestRepositoryBuilder slug(String slug) {
        this.slug = slug;
        return this;
    }

    public Repository build() {
        InternalRepository repo = builder.build();
        if (slug != null) {
            TestBuilderUtils.setPrivateField(repo, "slug", slug);
        }
        return repo;
    }
}
